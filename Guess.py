secret_number = 21

guess_count = 0
guess_limit = 3

while guess_count < guess_limit:
    guess = int(input("Guess: "))

    if guess == secret_number:
        print("You  win!")
        break

    guess_count += 1

else:
    # In Python3 our while loops can have else statements hence there is no
    # waste resorces adding another if to check equality given below
    print("Sorry You Failed")

# if guess != secret_number :
#     print("You Lose!")
