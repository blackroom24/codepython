import string
import secrets


def pass_gen():
    alphabet = string.ascii_letters + string.digits + string.punctuation
    while True:
        password = "".join(secrets.choice(alphabet) for i in range(12))
        if(any(c.islower() for c in password)
           and any(c.isupper() for c in password)
           and sum(c.isdigit()for c in password)):
            break
    return password


print(pass_gen())
