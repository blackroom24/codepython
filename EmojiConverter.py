def emoji_converter(message):
    words = message.split(' ')
    emoji = {
        ':)': "\U0001F600"  # Unicode for smiley
    }
    out = ""
    for word in words:
        out += emoji.get(word, word) + " "
    return out


message = input(">>> ")
print(emoji_converter(message))
