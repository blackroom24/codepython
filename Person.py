class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def talk(self):
        print(f"Hi,I am {self.first_name} {self.last_name}")


person_one = Person("Sanket", "Debnath")
person_one.talk()
